FROM node:10.15.3
COPY . ./
RUN npm install
ENV HOST=0.0.0.0
EXPOSE 3000
CMD ["npm", "start"]