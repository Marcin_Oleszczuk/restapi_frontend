import 'bootstrap/dist/css/bootstrap.min.css'
import React from 'react';
import './App.css';
import axios from 'axios';
import {Link} from 'react-router-dom'
import queryString from 'query-string'

class App extends React.Component {
  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = { FoldersList: []};
    this.SendRequest = this.SendRequest.bind(this);
    console.log('CONSTRUCTOR', props)
  }

  componentDidMount() {
    this.SendRequest()
  }

  SendRequest() {
    const queryParameters = queryString.parse(this.props.location.search)
    let url ='http://localhost:8085/'
    if (queryParameters) {
      url = `http://localhost:8085/${this.props.location.search}`
    }
    axios.get(url, {
            headers: {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
          }
    }) 
    .then(response => {
      this.setState((previousState, props) => {
        return {FoldersList : response.data.result};
      });
    })
    .catch(error => {
      console.log(error);
    });
  }
  render() {
      return (
          <table className="table table-dark">
            <thead>
              <tr>
                <th scope="col">id:</th>
                <th scope="col">path:</th>
              </tr>
            </thead>
            <tbody>
              {this.state.FoldersList.map((obj, index) => {
                return( <tr key={obj.id}>
                 <td> <Link to={`/${obj.id}`} >{obj.id} </Link></td>
                  <td>{obj.path}</td>
                </tr>)
              })}
            </tbody>
          </table>
      );
  }
}

export default App;
