import React from 'react';
import axios from 'axios';
import JSONPretty from 'react-json-pretty';
import JSONPrettyMon from 'react-json-pretty/dist/monikai';

class Details extends React.Component {
  constructor(props) {
    super(props);
    this.SendDetailsRequest = this.SendDetailsRequest.bind(this);
    this.state = { folder: {}};
  }

  componentDidMount() {
    this.SendDetailsRequest(this.props.match.params.id)
  }

  SendDetailsRequest(id) {
    axios.get(`http://localhost:8085/${id}`, {
            headers: {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods' : 'GET,PUT,POST,DELETE,PATCH,OPTIONS'
          }
    }) 
    .then(response => {
      this.setState((previousState, props) => {
        return {folder : response.data};
      });
    })
    .catch(error => {
      console.log(error);
    });
  }
  render() {
      return (
        <JSONPretty data={this.state.folder} theme={JSONPrettyMon}/>
        
      );
  }
}

export default Details;
